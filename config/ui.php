<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Container
    |--------------------------------------------------------------------------
    |
    | This value defines which Bootstrap container will be used for the <main>
    | element. Set the value to null for the default container.
    |
    | Supported: null, "sm", "md", "lg", "xl", "fluid"
    |
    */

    'container' => null,

    'navbar' => [
        /*
        |----------------------------------------------------------------------
        | Navbar Collapsible
        |----------------------------------------------------------------------
        |
        | This value defines whether the navbar is collapsibile into a
        | hamburger menu on small screens.
        |
        */

        'collapsible' => false,

        /*
        |----------------------------------------------------------------------
        | Navbar Expand
        |----------------------------------------------------------------------
        |
        | This value defines the breakpoint at which the navbar will expand
        | to full size. The navbar will collapse into a hamburger menu at
        | smaller breakpoints.
        |
        | Supported: "sm", "md", "lg", "xl", "xxl"
        |
        */

        'expand' => 'sm',
    ],

    /*
    |--------------------------------------------------------------------------
    | Dusk
    |--------------------------------------------------------------------------
    |
    | Indicates whether a Dusk test is currently running, so that frontend
    | features such as Turbo and client-side form validation that are
    | inconvenient for Dusk can be disabled.
    |
    */

    'dusk' => env('UI_DUSK', false),
];
