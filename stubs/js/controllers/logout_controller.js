/* global Turbolinks */

import { Controller } from "stimulus"
import { RemoteForm } from "../remote_form"

export default class extends Controller {
  static targets = ["form"]

  initialize() {
    this.remoteForm = new RemoteForm(this.formTarget, () => {
      Turbolinks.visit("/")
    })
  }

  submit(event) {
    event.preventDefault()
    this.remoteForm.submit()
  }
}
