/* global Turbolinks */

import { Controller } from "stimulus"
import { RemoteForm } from "../remote_form"
import { bootstrapAlert } from "../bootstrap_alert"

export default class extends Controller {
  static targets = ["form"]

  initialize() {
    this.remoteForm = new RemoteForm(this.formTarget, (response) => {
      if (response.status === 413) {
        bootstrapAlert("Your uploaded file is too large.")
        return
      }

      response.json().then((data) => {
        if (data.location) {
          Turbolinks.visit(data.location)
        } else if (data.html && data.selector) {
          const element = document.querySelector(data.selector)
          element.innerHTML = data.html
          element.scrollIntoView()
        } else {
          console.log(data)
          bootstrapAlert("Invalid response.")
        }
      })
    })
  }

  submit(event) {
    event.preventDefault()
    this.remoteForm.submit()
  }
}
