export function bootstrapAlert(message) {
  const alert = document.createElement("div")
  alert.classList.add("alert", "alert-danger")
  alert.innerHTML = message

  const main = document.querySelector("main")

  main.prepend(alert)
}
