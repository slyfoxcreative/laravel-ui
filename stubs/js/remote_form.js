/* global Turbolinks */

import { bootstrapAlert } from "./bootstrap_alert"

export class RemoteForm {
  constructor(form, postHandler) {
    this.form = form
    this.postHandler = postHandler
    this.disabled = false
  }

  submit() {
    if (this.disabled) {
      return
    }
    this.disabled = true
    this.form
      .querySelectorAll("button")
      .forEach((e) => e.classList.add("disabled"))

    if (document.querySelector("body").dataset.environment === "testing") {
      // Submit the form normally in Dusk tests
      this.form.submit()
    } else if (this.form.method.toLowerCase() === "get") {
      this.get()
    } else {
      this.post()
    }
  }

  get() {
    const queryString = new URLSearchParams(new FormData(this.form)).toString()
    Turbolinks.visit(`${this.form.action}?${queryString}`)
  }

  post() {
    const headers = new Headers()
    headers.append("Accept", "application/json")

    fetch(this.form.action, {
      method: "POST",
      headers: headers,
      body: this.data(),
    })
      .then((response) => {
        if (response.status === 403 || response.status === 500) {
          response.json().then((data) => {
            bootstrapAlert(data.message)
          })
          return
        }

        if (response.ok) {
          Turbolinks.clearCache()
        }

        this.postHandler(response)
      })
      .finally(() => {
        this.disabled = false
        this.form
          .querySelectorAll("button")
          .forEach((e) => e.classList.remove("disabled"))
      })
  }

  data() {
    const elements = Array.from(this.form.elements).filter((element) => {
      return (
        (element.type !== "checkbox" && element.type !== "radio") ||
        element.checked
      )
    })

    return elements.reduce((data, element) => {
      data.append(
        element.name,
        element.type === "file" && element.files[0]
          ? element.files[0]
          : element.value,
      )

      return data
    }, new FormData())
  }
}
