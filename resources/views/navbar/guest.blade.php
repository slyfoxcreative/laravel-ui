@includeIf('navbar.guest_menu')
<ul class="navbar-nav ms-auto">
  <li class="nav-item">
    <a class="nav-link" href="{{ route('login') }}">
      @icon('right-to-bracket')
      Sign in
    </a>
  </li>
</ul>
