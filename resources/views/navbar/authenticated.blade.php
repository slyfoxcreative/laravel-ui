@includeIf('navbar.authenticated_menu')
<span class="navbar-text ms-auto me-3">
  {{ auth()->user()->name }}
</span>
<x-html-form class="d-flex" :action="route('logout')" method="post">
  <x-bs-button style="outline-secondary">
    @icon('right-from-bracket')
    Sign out
  </x-bs-button>
</x-html-form>
