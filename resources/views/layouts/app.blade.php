<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
      {{ config('app.name', 'Laravel') }}@hasSection('title') - @yield('title')@endif
    </title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" data-turbo-track="reload">
    <script src="{{ mix('js/manifest.js') }}" defer data-turbo-track="reload"></script>
    <script src="{{ mix('js/vendor.js') }}" defer data-turbo-track="reload"></script>
    <script src="{{ mix('js/app.js') }}" defer data-turbo-track="reload"></script>
    @if (app()->isLocal())
      <script async src="http://localhost:3000/browser-sync/browser-sync-client.js"></script>
      <meta name="dusk" content="{{ config('ui.dusk') }}">
    @endif
    @if (config('broadcasting.default') === 'pusher')
      <meta name="pusher-key" content="{{ config('broadcasting.connections.pusher.key') }}">
      <meta name="pusher-host" content="{{ config('broadcasting.connections.pusher.frontend.host') }}">
      <meta name="pusher-port" content="{{ config('broadcasting.connections.pusher.frontend.port') }}">
    @endif
    @stack('head')
  </head>
  <body data-environment="{{ app()->environment() }}">
    <x-bs-navbar :collapsible="config('ui.navbar.collapsible')" :expand-size="config('ui.navbar.expand')" style="light" color="light">
      <x-slot name="brand">
        {{ config('app.name', 'Laravel') }}
      </x-slot>
      @guest
        @includeFirst(['navbar.guest', 'ui::navbar.guest'])
      @endguest
      @auth
        @includeFirst(['navbar.authenticated', 'ui::navbar.authenticated'])
      @endauth
    </x-bs-navbar>
    <main class="{{ $uiContainerClass }} mb-5 mt-4" id="app">
      @include('bootstrap::alerts')
      @if (Breadcrumbs::exists())
        {{ Breadcrumbs::render() }}
      @endif
      @yield('main')
    </main>
  </body>
</html>
