<?php

declare(strict_types=1);

namespace SlyFoxCreative\Ui;

use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        $uiContainerClass = is_null(config('ui.container'))
            ? 'container'
            : 'container-' . config('ui.container');

        $package
            ->name('laravel-ui')
            ->hasConfigFile()
            ->hasViews()
            ->sharesDataWithAllViews('uiContainerClass', $uiContainerClass)
        ;
    }

    public function packageBooted(): void
    {
        $this->publishes([
            __DIR__ . '/../stubs/js/' => resource_path('js'),
        ], 'ui-resources');
    }
}
