<?php

declare(strict_types=1);

require 'vendor/autoload.php';

if (is_readable(__DIR__ . '/../.env')) {
    Dotenv\Dotenv::createImmutable(__DIR__, '/../.env')->load();
}
